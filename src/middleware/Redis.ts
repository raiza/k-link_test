import Data from '@entities/common/Data';
import ResponseData from '@entities/common/ResponseData';
import { NextFunction, Request, Response } from 'express';
import redis, { ClientOpts } from 'redis';

const redisOption: ClientOpts = {
  host: process.env.REDIS_HOST,
  port: parseInt(process.env.REDIS_PORT),
  password: process.env.REDIS_PASSWORD
}
const client = redis.createClient(redisOption)

export function caching(req: Request, res: Response, next: NextFunction) {
  const param = JSON.stringify(req.query)

  client.get(param, (err, result) => {
    if (err) throw err;

    if (result != null) {
      const data = new Data()
       Object.assign(data, JSON.parse(result));
      return res.status(200).json(new ResponseData(200, data))
    } else {
      return next()
    }
  })
}

export function saveCache(req: Request, duration: number, value: string) {
  const param = JSON.stringify(req.query)
  client.setex(param,duration,value)
}