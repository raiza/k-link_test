import User from "@models/User";
import { Strategy as LocalStrategy } from "passport-local";
import { Strategy as BearerStrategy } from "passport-http-bearer";
import { ExtractJwt, Strategy as JWTStrategy } from "passport-jwt";
import { Response } from "express";
import passpor from "passport";
import { PassportStatic } from "passport";
import { IRequestUser, IUser } from "@entities/User";
import Error from "@entities/common/Error";
import ResponseData from "@entities/common/ResponseData";
import Admin from "@models/Admin.model";
import jwt from "jsonwebtoken";
import { ITokenData } from "@entities/common/TokenData";
import { Request } from "@entities/common/types";

export const localAuthUser = (passport: PassportStatic) => {
  passport.use(
    "user-local",
    new LocalStrategy(
      {
        usernameField: "email",
        passwordField: "password",
        session: false,
      },
      async (username, password, done) => {
        try {
          const user = await User.findOne({
            where: {
              email: username.toLowerCase(),
            },
          });

          if (user != null) {
            const match = await user.validatePassword(password);
            if (match) {
              return done(null, user.toJSON());
            } else {
              return done([
                new Error("Email or Password not Match", "password"),
              ]);
            }
          } else {
            return done([new Error("Email or Password not Match", "email")]);
          }
        } catch (error) {
          return done(error);
        }
      }
    )
  );
};

export const localAuthAdmin = (passport: PassportStatic) => {
  passport.use(
    "admin-local",
    new LocalStrategy(
      {
        usernameField: "email",
        passwordField: "password",
        session: false,
      },
      async (username, password, done) => {
        try {
          const admin = await Admin.findOne({
            where: {
              email: username.toLowerCase(),
            },
          });

          if (admin != null) {
            const match = await admin.validatePassword(password);
            if (match) {
              return done(null, admin.toJSON());
            } else {
              return done([
                new Error("Email or Password not Match", "password"),
              ]);
            }
          } else {
            return done([new Error("Email or Password not Match", "email")]);
          }
        } catch (error) {
          return done(error);
        }
      }
    )
  );
};

export const bearerAuth = (passport: PassportStatic) => {
  passport.use(
    "Bearer",
    new BearerStrategy(async (token, done) => {
      try {
        const decoded = jwt.verify(token, process.env.SECRET) as ITokenData;

        if (decoded.type == "admin") {
          const admin = await Admin.findByPk(decoded.id);
          return done(null, admin.toJSON(), "admin");
        } else {
          const user = await User.findByPk(decoded.id);
          return done(null, user.toJSON(), "user");
        }
      } catch (error) {
        return done([new Error("Unauthorized")]);
      }
    })
  );
};

export const bearerAuthQuery = (passport: PassportStatic) => {
  passport.use(
    new JWTStrategy(
      {
        secretOrKey: process.env.SECRET,
        jwtFromRequest: ExtractJwt.fromUrlQueryParameter("token"),
      },
      async (token, done) => {
        try {
          const decoded = token as ITokenData;

          if (decoded.type == "admin") {
            const admin = await Admin.findByPk(decoded.id);
            return done(null, admin.toJSON(), "admin");
          } else {
            const user = await User.findByPk(decoded.id);

            return done(null, user.toJSON(), "user");
          }
        } catch (error) {
          return done([new Error("Unauthorized")]);
        }
      }
    )
  );
};

export const isLoggedIn = (
  req: IRequestUser,
  res: Response,
  next: (error?: any) => void
) => {
  passpor.authenticate(
    "Bearer",
    { session: false },
    function (err, user, info) {
      if (err) {
        return res
          .status(200)
          .json(new ResponseData(401, null, null, [new Error("Unauthorized")]));
      }
      if (!user) {
        return res
          .status(200)
          .json(new ResponseData(401, null, null, [new Error("Unauthorized")]));
      }

      if (info == "admin") {
        req.admins = user;
      } else {
        req.users = user;
      }

      return next();
    }
  )(req, res, next);
};

export const isLoggedInDownload = (
  req: IRequestUser,
  res: Response,
  next: (error?: any) => void
) => {
  passpor.authenticate("jwt", { session: false }, function (err, user, info) {
    if (err) {
      return res
        .status(200)
        .json(new ResponseData(401, null, null, [new Error("Unauthorized")]));
    }
    if (!user) {
      return res
        .status(200)
        .json(new ResponseData(401, null, null, [new Error("Unauthorized")]));
    }

    if (info == "admin") {
      req.admins = user;
    } else {
      req.users = user;
    }

    return next();
  })(req, res, next);
};

export const isAdmin = (
  req: Request,
  res: Response,
  next: (error?: any) => void
) => {
  if (req.admins != null) {
    return next();
  } else {
    return res
      .status(200)
      .json(
        new ResponseData(400, null, null, [new Error("Restricted access")])
      );
  }
};

export const isUser = (
  req: Request,
  res: Response,
  next: (error?: any) => void
) => {
  if (req.users != null) {
    return next();
  } else {
    return res
      .status(200)
      .json(
        new ResponseData(400, null, null, [new Error("Restricted access")])
      );
  }
};
