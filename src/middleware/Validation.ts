import { IChangePasswordRequest } from "@entities/common/ChangePassword";
import Error from "@entities/common/Error";
import ResponseData from "@entities/common/ResponseData";
import Admin from "@models/Admin.model";
import User from "@models/User";
import { generateHash } from "@shared/functions";
import { Request, Response } from "express";
import { body, param, validationResult } from "express-validator";

export const validationUUID = async (
  req: Request,
  res: Response,
  next: (error?: any) => void
) => {
  Promise.all([
    await param("id").isUUID().withMessage("id not valid").run(req),
  ]);

  const result = validationResult(req);

  if (!result.isEmpty()) {
    return res.json(new ResponseData(400, null, null, result.array()));
  }

  return next();
};

export const validationId = async (
  req: Request,
  res: Response,
  next: (error?: any) => void
) => {
  Promise.all([
    await param("id").notEmpty().withMessage("Id is required").run(req),
  ]);

  const result = validationResult(req);

  if (!result.isEmpty()) {
    return res.json(new ResponseData(400, null, null, result.array()));
  }

  return next();
};

export const validationChangePassword = async (
  req: IChangePasswordRequest,
  res: Response,
  next: (error?: any) => void
) => {
  Promise.allSettled([
    await body("currentPassword")
      .isLength({ min: 6 })
      .withMessage("The password min 6 characters")
      .custom(async (value) => {
        try {
          if (req.users != null) {
            const user = await User.findByPk(req.users.id);

            if (user == null) {
              throw "User not found";
            }

            const match = await user.validatePassword(value);
             if (!match) {
              throw "Current password is wrong";
            }
          } else {
            const admin = await Admin.findByPk(req.admins.id);

            if (admin == null) {
              throw "Admin not found";
            }

            const match = await admin.validatePassword(value);
            if (!match) {
              throw "Current password is wrong";
            }
          }
        } catch (error) {
          throw error;
        }

        return true;
      })
      .run(req),
    await body("newPassword")
      .isLength({ min: 6 })
      .withMessage("The password min 6 characters")
      .run(req),
    await body("newPasswordConfirmation")
      .custom((value, { req }) => {
        if (value !== req.body.newPassword) {
          throw "New password confirmation does not match New password";
        }
        return true;
      })
      .run(req),
  ]);

  const result = validationResult(req);

  if (!result.isEmpty()) {
    return res.json(new ResponseData(400, null, null, result.array()));
  }

  const password = await generateHash(req.body.newPassword);
  req.body.newPassword = password;

  return next();
};
