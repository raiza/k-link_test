
module.exports = {
  up: (queryInterface,Sequelize) => {
    return queryInterface.addColumn('Shares','reportTypeId', {
      type: Sequelize.UUID,
      onDelete:"CASCADE",
      onUpdate:"CASCADE",
      references: {
          model: 'ReportTypes', // name of Target model
          key: 'id', // key in Target model that we're referencing
        }
      });
  }
};