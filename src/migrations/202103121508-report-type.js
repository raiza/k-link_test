module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ReportTypes', {
      id: {
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      }
    }).success(() => {
      queryInterface.migrator.sequelize.query(`INSERT INTO "public"."ReportTypes"("id", "name", "createdAt", "updatedAt") VALUES ('e2a16d48-aedd-4563-9bef-8ceec4089927', 'melon', '2021-03-08 16:29:29.301+07', '2021-03-08 16:29:29.301+07'),('3c30ac22-14ea-4e04-b8e1-cfefa6c94afb', 'Telkomsel', '2021-03-09 09:49:32.364+07', '2021-03-09 09:49:32.364+07'), ('b916d118-5b56-406e-bd70-73ab66c510d0', 'Indosat', '2021-03-12 09:38:17.476+07', '2021-03-12 09:38:17.476+07'), ('907da856-853c-4eab-871e-4ef7fa5916bd', 'XL', '2021-03-12 14:12:40.361+07', '2021-03-12 14:12:40.361+07'), ('f86ecb5e-33f2-4feb-a5f3-82d661b81236', 'Tri', '2021-03-12 15:13:49.762+07', '2021-03-12 15:13:49.762+07'), ('2151cbc9-09a1-435d-8386-c14982e2de52', 'warner', '2021-03-08 16:29:29.301+07', '2021-03-08 16:29:29.301+07');`);
      done();
    });
  },
  down: (queryInterface, Sequelize) => {
    queryInterface.dropTable(
      'ReportTypes'
    ).then(function() {
      done();
    })
  }
};