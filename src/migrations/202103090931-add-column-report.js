
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Reports', 'reportDate', {
      type: Sequelize.DATEONLY,
    });
  }
};