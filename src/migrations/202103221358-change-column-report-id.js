
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('Reports', 'id', {
      primaryKey: true,
      allowNull: false,
      unique:'id_store_id',
      type: Sequelize.STRING,
    });
  }
};