
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('Reports', 'storeId', {
      primaryKey: true,
      allowNull: false,
      unique:'id_store_id',
      type: Sequelize.UUID,
      references: {
        model: 'ReportTypes', // name of Target model
        key: 'id', // key in Target model that we're referencing
      }
    });
  }
};