
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Reports', 'storeId', {
      primaryKey: true,
      allowNull: false,
      type: Sequelize.UUID,
      references: {
        model: 'ReportTypes', // name of Target model
        key: 'id', // key in Target model that we're referencing
      }
    });
  }
};