
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('Reports', 'amount', {
      type: Sequelize.DECIMAL(17, 8),
    });
  }
};