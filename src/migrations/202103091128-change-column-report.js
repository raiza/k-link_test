
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('Reports', 'id', {
      primaryKey: true,
      allowNull: false,
      type: Sequelize.STRING,
    });
  }
};