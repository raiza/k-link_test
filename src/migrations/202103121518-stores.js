module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Stores', {
      id: {
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      tag: {
        type: Sequelize.STRING,
        allowNull: false
      },
      reportTypeId: {
        type: Sequelize.UUID,
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
        references: {
          model: 'ReportTypes', // name of Target model
          key: 'id', // key in Target model that we're referencing
        }
      }
    }).success(() => {
      queryInterface.migrator.sequelize.query(`INSERT INTO "public"."Stores"("id", "name", "tag", "reportTypeId", "createdAt", "updatedAt") VALUES ('910a3724-c7be-41e1-88f2-bd0b0565e50d', 'Melon', 'Melon', 'e2a16d48-aedd-4563-9bef-8ceec4089927', '2021-03-08 16:30:14.657+07', '2021-03-08 16:30:14.657+07'), ('94651dce-f856-4949-8094-38d8f1557593', 'Telkomsel', 'telkomsel,tsel', '3c30ac22-14ea-4e04-b8e1-cfefa6c94afb', '2021-03-09 09:50:03.253+07', '2021-03-09 09:50:03.253+07'), ('cc9ebe1a-35f9-4c1b-a644-487d07ba2386', 'Indosat', 'Indosat, Isat', 'b916d118-5b56-406e-bd70-73ab66c510d0', '2021-03-12 09:38:51.059+07', '2021-03-12 09:38:51.059+07'), ('13a2c939-8584-4567-b2d3-d85ff418c629', 'XL', 'XL', '907da856-853c-4eab-871e-4ef7fa5916bd', '2021-03-12 14:13:24.73+07', '2021-03-12 14:13:24.73+07'), ('0545913d-e325-43be-884d-127c54d68721', 'Tri (3)', 'Tri, 3', 'f86ecb5e-33f2-4feb-a5f3-82d661b81236', '2021-03-12 15:14:24.452+07', '2021-03-12 15:14:24.452+07');`);
      done();
    });
  },
  down: (queryInterface, Sequelize) => {
    queryInterface.dropTable(
      'Stores'
    ).then(function () {
      done();
    })
  }
};