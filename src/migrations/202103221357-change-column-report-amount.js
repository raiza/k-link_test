
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('Reports', 'amount', {
      primaryKey: true,
      allowNull: false,
      type: Sequelize.DECIMAL(27,0),
    });
  }
};