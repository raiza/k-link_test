
module.exports = {
  up: (queryInterface,Sequelize) => {
    return queryInterface.changeColumn('Users','userId', {
      type: Sequelize.UUID,
      onDelete:"CASCADE",
      onUpdate:"CASCADE",
      references: {
          model: 'Users', // name of Target model
          key: 'id', // key in Target model that we're referencing
        }
      });
  }
};