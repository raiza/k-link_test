import { UserType } from '@shared/constant';
import { Model, Table, Column, DataType, BelongsTo,Length, ForeignKey, HasMany, IsEmail } from "sequelize-typescript";
import bcrypt from "bcrypt";
import { IUser } from '@entities/User';
// import Share from '@models/Share.model';

@Table
class User extends Model implements IUser {
  @Column({
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
    type: DataType.UUID
  }) 
  id: string;

  @IsEmail
  @Column({
    allowNull: false,
    unique: true,
  })
  email: string;

  @Column({
    allowNull: false,
  })
  password: string;

  @Column({
    allowNull: false,
  })
  name: string;
  
  @Length({ min: 10, max: 20 })
  @Column({
    type: DataType.STRING(20),
    allowNull: false,
  })
  identityNumber: string;

  @Length({ min: 5, max: 16 })
  @Column({
    type: DataType.STRING(16),
    allowNull: false,
  })
  phoneNumber: number;

  @Length({ min: 20, max: 20 })
  @Column({
    type: DataType.STRING(20),
    allowNull: false,
  })
  npwp: string;

  @Column({
    allowNull: false,
  })
  CAN: string;

  @Column({
    allowNull: false,
  })
  accountNumber: string;

  @Column({
    allowNull: false,
  })
  accountName: string;

  @Column({
    allowNull: false
  })
  type: UserType

  @ForeignKey(() => User)
  @Column(DataType.UUID)
  userId: string;

  @BelongsTo(() => User, {
    onUpdate: "CASCADE",
    onDelete: "NO ACTION",
  })
  user: User;

  // @HasMany(() => Share, {
  //   onUpdate: "CASCADE",
  //   onDelete: "CASCADE",
  // })
  // shares: Share[];

  toJSON() {
    const values = Object.assign({password: null}, this.get());
    delete values.password
    return values;
  }

  validatePassword(password: string) {
    return bcrypt.compare(password, this.password);
  }
}

export default User;
