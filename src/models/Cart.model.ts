// import { MasterProduct } from '@models/MasterProduct.model';
import { ICart } from '@entities/Cart';
import { Model, Table, Column, DataType, ForeignKey, BelongsTo, HasMany } from "sequelize-typescript";
import MasterProduct from '@models/MasterProduct.model';
import Checkout from '@models/Checkout.model';
import User from '@models/User';
@Table
class Cart extends Model implements ICart {
  @Column({
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
    type: DataType.UUID,
  })
  id: string;

  // @Column({
  //   allowNull: false,
  // })
  // name: string;

  @Column({
    allowNull: false,
  })
  qty: number;

  @ForeignKey(() => User)
  @Column(DataType.UUID)
  userId: string;

  @BelongsTo(() => User)
  user: User;

  @ForeignKey(() => MasterProduct)
  @Column(DataType.UUID)
  masterProductId: string;

  @BelongsTo(() => MasterProduct)
  masterProduct: MasterProduct;

  @HasMany(() => Checkout, {
    onUpdate: "CASCADE",
    onDelete: "CASCADE",
  })
  checkout: Checkout[];
}

export default Cart;
