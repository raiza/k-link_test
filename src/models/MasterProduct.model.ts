
import { IMasterProduct } from "@entities/MasterProduct";
import { Model, Table, Column, DataType, HasMany } from "sequelize-typescript";

import Cart from '@models/Cart.model';
@Table
class MasterProduct extends Model implements IMasterProduct {
  @Column({
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
    type: DataType.UUID,
  })
  id: string;

  @Column({
    allowNull: false,
  })
  name: string;

  @Column({
    allowNull: false,
  })
  price: number;

  @Column({
    allowNull: false,
  })
  qty: number;

  
  @HasMany(() => Cart, {
    onUpdate: "CASCADE",
    onDelete: "CASCADE",
  })
  cart: Cart[];
}

export default MasterProduct;
