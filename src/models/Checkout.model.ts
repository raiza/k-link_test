

import { ICheckout } from "@entities/Checkout";
import { Model, Table, Column, DataType, ForeignKey, BelongsTo } from "sequelize-typescript";
import User from '@models/User';
import Cart from '@models/Cart.model';

@Table
class Checkout extends Model implements ICheckout {
  @Column({
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
    type: DataType.UUID,
  })
  id: string;

  @Column({
    allowNull: false,
  })
  name: string;

  @Column({
    allowNull: false,
  })
  price: number;

  @Column({
    allowNull: false,
  })
  qty: number;

  
  @ForeignKey(() => User)
  @Column(DataType.UUID)
  userId: string;

  @BelongsTo(() => User)
  user: User;

  @ForeignKey(() => Cart)
  @Column({
    type:DataType.UUID,
    allowNull: false,
  })
  cartId: string;

  @BelongsTo(() => Cart)
  cart: Cart;
}

export default Checkout;
