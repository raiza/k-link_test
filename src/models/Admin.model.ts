import { IAdmin } from '@entities/Admin';
import { Model, Table, Column, DataType, BelongsTo,Length } from "sequelize-typescript";
import bcrypt from "bcrypt";

@Table
class Admin extends Model implements IAdmin {
  @Column({
    defaultValue: DataType.UUIDV4,
    primaryKey: true,
    type: DataType.UUID
  }) 
  id: string;

  @Column({
    allowNull: false,
    unique: true,
  })
  email: string;

  @Column({
    allowNull: false,
  })
  password: string;

  @Column({
    allowNull: false,
  })
  name: string;

  toJSON() {
    const values = Object.assign({password: null}, this.get());
    delete values.password
    return values;
  }

  validatePassword(password: string) {
    return bcrypt.compare(password, this.password);
  }
}

export default Admin;
