
import { Request } from "express";

export interface IMasterProduct {
  id: string;
  name: string;
  price: number;
  qty: number;
  
}

export interface IMasterProductRequest extends Request {
  body: IMasterProduct;
}


