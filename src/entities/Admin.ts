import { Request } from "express";

export interface IAdmin {
  id: string;
  email: string;
  password: string;
  name: string;
}

export interface IRequestAdmin extends Request {
  body: IAdmin;
}
