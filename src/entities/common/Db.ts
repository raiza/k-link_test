import { Sequelize } from "sequelize/types";

export interface IDb {
  sql: Sequelize
}