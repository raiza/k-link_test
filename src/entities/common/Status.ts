export interface IStatus {
  code: number,
  message: string
}

class Status implements IStatus {
  code: number;
  message: string;

  constructor(code: number, message: string) {
    this.code = code
    this.message = message
  }
}

export default Status