import { Request } from "express";

export interface IChangePassword {
  currentPassword: string;
  newPassword: string;
  newPasswordConfirmation: string;
}

export interface IChangePasswordRequest extends Request {
  body: IChangePassword;
}