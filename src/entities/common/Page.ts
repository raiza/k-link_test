export interface IPage {
  currentPage: number;
  limitPerPage: number;
  totalPage: number;
  total: number;
}

class Page implements IPage {
  currentPage: number;
  limitPerPage: number;
  totalPage: number;
  total: number;

  constructor(currentPage: number, limitPerPage: number, totalData: number) {
    this.currentPage = currentPage+1
    this.limitPerPage = limitPerPage
    this.totalPage = Math.ceil(totalData/limitPerPage)
    this.total = totalData
  }
}

export default Page
