import express from 'express';
import * as core from 'express-serve-static-core'

export interface Query extends core.Query { }

export interface Params extends core.ParamsDictionary { }

export interface Request<ReqBody = any, ReqQuery = Query, Params = core.ParamsDictionary>
  extends express.Request<Params, any, ReqBody, ReqQuery> {
  }