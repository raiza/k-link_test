export interface ILog {
  path: string;
  data: any;
  method: string;
}

class Log implements ILog {
  path: string
  data: any
  method: string

  constructor(path: string, method: string, data: any) {
    this.path = path
    this.method = method
    this.data = data
  }
}

export default Log
