export interface ITokenData {
  id: string;
  type: string;
}