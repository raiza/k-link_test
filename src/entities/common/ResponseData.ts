import Sequelize from "sequelize";
import { IData } from "@entities/common/Data";
import Status, { IStatus } from "@entities/common/Status";

export interface IResponseData {
  data: IData;
  errors: any[];
  error: any;
  status: IStatus;
}

class ResponseData implements IResponseData {
  errors: any[];
  error: any;
  data: IData;
  status: IStatus;

  constructor(statusCode: number, data?: IData, error?: any, errors?: any[]) {

    switch (statusCode) {
      case 200:
        this.status = new Status(statusCode, "Success Get Data");
        break;
      case 201:
        this.status = new Status(statusCode, "Success Create Data");
        break;
      case 400:
        this.status = new Status(statusCode, "error");
        break;
      default:
        this.status = new Status(statusCode, "unauthorized");
        break;
    }

    if (errors instanceof Sequelize.ForeignKeyConstraintError) {
      this.errors = [
        {
          message: "not found",
          param: errors.fields[0],
          value: errors.value,
        },
      ];
    } else if (
      errors instanceof Sequelize.UniqueConstraintError ||
      errors instanceof Sequelize.ValidationError
    ) {
      this.errors = errors.errors;
    } else if (errors instanceof Sequelize.DatabaseError) {
      var msg = errors.parent.message;
      this.errors = [
        {
          message: msg,
          param: msg.substring(msg.lastIndexOf("'"), msg.indexOf("'") + 1),
          value: "",
        },
      ];
    } else {
      this.errors = errors;
    }

    if (data == null) {
      delete this.data
    } else {
      this.data = data
    }

    if (error == null) {
      delete this.error
    } else {
      this.error = error
    }

    if (errors == null) {
      delete this.errors
    } else {
      this.errors = errors
    }
  }
}

export default ResponseData;
