export interface IError {
  msg: string;
  param: string;
  value: string
}

class Error implements IError {
  msg: string;
  param: string;
  value: string;

  constructor(msg: string, param?: string, value?: string) {
    this.msg = msg
    this.param = param
    this.value = value
  }
}

export default Error