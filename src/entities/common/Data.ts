import Page from "@entities/common/Page";

export interface IData {
  paging: Page
}

class Data implements IData {
  paging: Page;
  constructor(key?: string[],data?: any[], paging?: Page) {
    if (key != null) {
      // Object.assign(this, { [key]: data });
      key.map((item,i) => {
        return  Object.assign(this, {[item]:data[i]});
      })
    }
    if (paging != null) {
      this.paging = paging
    } else {
      delete this.paging
    }
  }
}

export default Data