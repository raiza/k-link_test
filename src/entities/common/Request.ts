import { Query, Request } from "@entities/common/types";

export interface IQueryRequest extends Query {
  page: string;
  q: string;
  perPage: string;
  orderBy: string[];
}

export interface IRequestChangePassword extends Body {
  page: string;
  perPage: string;
  orderBy: string[];
}
