

import MasterProduct from '@models/MasterProduct.model';
import Checkout from '@models/Checkout.model';
import User from '@models/User';
import { Request } from "express";

export interface ICart {
  id: string;
  qty: number;
  userId: string;
  user: User
  masterProductId: string;
  masterProduct: MasterProduct
  checkout: Checkout[];
}

export interface ICartRequest extends Request {
  body: ICart;
}
