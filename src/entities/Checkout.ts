

import User from '@models/User';
import { Request } from "express";

export interface ICheckout {
    id: string;
    name: string;
    price: number;
    qty: number;
  userId: string;
  user: User
 
}

export interface ICheckoutRequest extends Request {
  body: ICheckout;
}

export default class Checkout implements ICheckout {
  id: string;
  name: string;
  price: number;
  qty: number;
userId: string;
user: User
  constructor(
    name: string,
  price: number,
  qty: number,
userId: string,) {
    this.name = name
    this.price = price
    this.userId = userId
    this.qty = qty
  }

}
