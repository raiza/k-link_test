import { UserType } from "@shared/constant";
import { Request } from "express";
// import Share from '@models/Share.model';
import User from '@models/User';
import { IQueryRequest } from "@entities/common/Request";

export interface IUser {
  id: string;
  email: string;
  password: string;
  name: string;
  identityNumber: string;
  phoneNumber: number;
  npwp: string;
  CAN: string; //Cooperation Agreement Number
  accountNumber: string;
  accountName: string;
  type: UserType;
  userId: string;
  user: User
  // shares: Share[]
}

export interface IRequestUser extends Request {
  body: IUser;
}

export interface IQueryRequestUser extends IQueryRequest {
  type: UserType;
  userId: string;
}
