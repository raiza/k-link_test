export enum Gender {
  MALE = "Male",
  FEMALE = "Female",
}

export enum UserType {
  LABEL = "label",
  ARTIST = "artist",
  PRODUCER = "producer",
  BROKER = "broker",
}

export enum ModelKey {
  USER = "user",
  CLASSROOMS = "classrooms",
}

export enum ReportTypeList {
  MELON = "melon",
  INDOSAT = "indosat",
  TSEL = "telkomsel",
  TRI = "tri",
  JOOX = "joox",
  WARNER = "warner",
  XL = "xl"
}

export enum ReportTypeFirstColumn {
  MELON = "TERRITORY",
  TSEL = "BILLINGCYCLE",
  INDOSAT_1 = "Date",
  INDOSAT_2 = "LABEL_CODE",
  TRI = "LABEL",
  WARNER = "Recdate Month ID",
  XL_1 = "Title",
  XL_2 = "Music Label",
  XL_3 = "SOUND",
}
