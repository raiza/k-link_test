import logger from '@shared/Logger';
import bcrypt from "bcrypt";
import crypto from 'crypto'

export const pErr = (err: Error) => {
    if (err) {
        logger.err(err);
    }
};

// export const getRandomInt = () => {
//     return Math.floor(Math.random() * 1_000_000_000_000);
// };

export const randomValueHex = (len: number) => {
  return crypto.randomBytes(Math.ceil(len/2))
      .toString('hex') // convert to hexadecimal format
      .slice(0,len).toUpperCase();   // return required number of characters
}

export const generateHash = (password: string) => {
  const saltRounds = Number(process.env.SALT_ROUND);
  return bcrypt.hash(password, bcrypt.genSaltSync(saltRounds));
}
