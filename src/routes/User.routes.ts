import { Op } from 'sequelize';
import { validationChangePassword, validationUUID } from "@middleware/Validation";
import Data from "@entities/common/Data";
import ResponseData from "@entities/common/ResponseData";
import { IQueryRequestUser, IRequestUser } from "@entities/User";
import { Response, Router } from "express";
import User from "@models/User";
import Error from "@entities/common/Error";
import { caching, saveCache } from "@middleware/Redis";
import { Params, Request } from "@entities/common/types";
import Page from "@entities/common/Page";
import { IChangePasswordRequest } from "@entities/common/ChangePassword";
import logger from "@shared/Logger";
import Log from "@entities/common/Log";
import { Sequelize } from "sequelize-typescript";
const router = Router();

router.get(
  "/list",
  caching,
  async (req: Request<any, IQueryRequestUser, Params>, res: Response) => {
    try {
      var offset = 0;
      var limit = 10;
      var page = parseInt(req.query.page);
      var perPage = parseInt(req.query.perPage);
      var where: any = {};
      var whereInclude: any = {};
      var order: any = []

      if (perPage >= 1) {
        limit = perPage;
      }

      if (page > 1) {
        offset = page - 1;
        offset = offset * limit;
      }

      if (req.query.type != null) {
        where.type = req.query.type;
      }

      if (req.query.userId != null) {
        where.userId = req.query.userId;
        whereInclude.userId = req.users.id;
      } else {
        where.userId = req.users.id;
      }

      if (req.query.orderBy != null && req.query.orderBy.length > 0) {
        order = req.query.orderBy.map(ordering => [ordering.split(" ")[0],ordering.split(" ")[1]])
      }

      if (req.query.q != null) {
        where[Op.or] = [
          Sequelize.literal(`"User".*::TEXT ILIKE '%${req.query.q}%'`),
        ];
      }

      const {rows, count} = await User.findAndCountAll({
        offset: offset,
        limit: limit,
        where,
        include: [
          {
            model: User,
            where: whereInclude,
            attributes: [],
          },
        ],
        order: order,
      });

      if (rows != null && rows.length > 0) {
        const data = new Data(
          ["user"],
          [rows],
          new Page(offset, limit, count)
        );
        saveCache(req, 5, JSON.stringify(data));
        return res.status(200).json(new ResponseData(200, data));
      } else {
        return res.json(
          new ResponseData(400, null, null, [new Error("Data not found")])
        );
      }
    } catch (error) {
      return res.json(new ResponseData(400, null, null, error));
    }
  }
);

router.get("/self", caching, async (req: IRequestUser, res: Response) => {
  try {

    const user = await User.findByPk(
      req.users != null ? req.users.id : req.admins.id
    );
    if (user != null) {
      const data = new Data(["user"], [user]);
      saveCache(req, 5, JSON.stringify(data));

      return res.status(200).json(new ResponseData(200, data));
    } else {
      return res
        .status(200)
        .json(
          new ResponseData(400, null, null, [new Error("failed get User")])
        );
    }
  } catch (error) {
    return res.json(new ResponseData(400, null, null, error));
  }
});

router.get(
  "/:id",
  validationUUID,
  caching,
  async (req: IRequestUser, res: Response) => {
    var where: any = {};
    var whereInclude: any = {};
    where.id = req.params.id


    if (req.query.userId != null) {
      where.userId = req.query.userId;
      if (req.users != null) {
        whereInclude.userId = req.users.id;
      }
    } else if(req.users != null) {
      where.userId = req.users.id;
    }

    try {
      const user = await User.findOne({
        where,
          include: [
            {
              model: User,
              where: whereInclude,
              attributes: [],
            },
          ],
      });
      if (user != null) {
        const data = new Data(["user"], [user]);
        saveCache(req, 5, JSON.stringify(data));

        return res.status(200).json(new ResponseData(200, data));
      } else {
        return res
          .status(200)
          .json(
            new ResponseData(400, null, null, [new Error("failed get User")])
          );
      }
    } catch (error) {
      logger.warn(
        JSON.stringify(
          JSON.stringify(new Log(req.originalUrl, req.method, error))
        )
      );
      return res.json(new ResponseData(400, null, null, error));
    }
  }
);

router.put(
  "/changePassword",
  validationChangePassword,
  async (req: IChangePasswordRequest, res: Response) => {
    try {
      const data = await User.findByPk(req.users.id);
      if (data == null) {
        return res.json(
          new ResponseData(400, null, null, [new Error("User not found")])
        );
      }

      const user = await User.update(
        {
          password: req.body.newPassword,
        },
        {
          where: {
            id: req.users.id,
          },
          returning: true,
        }
      );

      if (user != null) {
        logger.info(
          JSON.stringify(new Log(req.originalUrl, req.method, user[1][0]))
        );
        return res.json(
          new ResponseData(200, new Data(["user"], [user[1][0]]))
        );
      } else {
        return res.json(
          new ResponseData(400, null, null, [
            new Error("failed change password User"),
          ])
        );
      }
    } catch (error) {
      logger.warn(
        JSON.stringify(
          JSON.stringify(new Log(req.originalUrl, req.method, error))
        )
      );
      return res.status(200).json(new ResponseData(400, null, null, error));
    }
  }
);

export default router;
