import { Sequelize } from "sequelize-typescript";
import logger from "@shared/Logger";
import Data from "@entities/common/Data";
import ResponseData from "@entities/common/ResponseData";
import { Response, Router } from "express";
import { body, param, validationResult } from "express-validator";
import { caching, saveCache } from "@middleware/Redis";
import { Params, Request } from "@entities/common/types";
import { IQueryRequest } from "@entities/common/Request";
import Page from "@entities/common/Page";
import Error from "@entities/common/Error";
import Store from "@models/Cart.model";
import { ICartRequest } from "@entities/Cart";
import MasterProduct from "@models/MasterProduct.model";
import { validationUUID } from "@middleware/Validation";
import Log from "@entities/common/Log";
import { Op } from "sequelize";
const router = Router();

router.get(
  "/list",
  caching,
  async (req: Request<any, IQueryRequest, Params>, res: Response) => {
    try {
      var offset = 0;
      var limit = 10;
      var page = parseInt(req.query.page);
      var perPage = parseInt(req.query.perPage);
      var order: any = [];
      var where: any = {};

      if (perPage >= 1) {
        limit = perPage;
      }

      if (page > 1) {
        offset = page - 1;
        offset = offset * limit;
      }

      if (req.query.orderBy != null && req.query.orderBy.length > 0) {
        order = req.query.orderBy.map((ordering) => [
          ordering.split(" ")[0],
          ordering.split(" ")[1],
        ]);
      }
      if (req.query.userId != null) {
        where.userId = req.query.userId;
      }
      if (req.query.q != null) {
        where[Op.or] = [

          Sequelize.literal(`"masterProduct"."name" ILIKE '%${req.query.q}%'`)
        ];
      }

      const { rows, count } = await Store.findAndCountAll({
        offset: offset,
        limit: limit,
        order: order,
        where,
        attributes: {
          include: [
            [Sequelize.literal('"masterProduct"."name"'), "masterProductName"],
          ],
        },
        include: [
          {
            model: MasterProduct,
            attributes: [],
          },
        ],
      });

      if (rows != null && rows.length > 0) {
        const data = new Data(
          ["cart"],
          [rows],
          new Page(offset, limit, count)
        );
        saveCache(req, 5, JSON.stringify(data));
        return res.status(200).json(new ResponseData(200, data));
      } else {
        return res
          .status(200)
          .json(
            new ResponseData(400, null, null, [new Error("Data not found")])
          );
      }
    } catch (error) {
      return res.json(new ResponseData(400, null, null, error));
    }
  }
);

router.get(
  "/:id",
  validationUUID,
  caching,
  async (req: ICartRequest, res: Response) => {
    try {
      const store = await Store.findByPk(req.params.id);
      if (store != null) {
        const data = new Data(["cart"], [store]);
        saveCache(req, 5, JSON.stringify(data));

        return res.json(new ResponseData(200, data));
      } else {
        return res.json(
          new ResponseData(400, null, null, [new Error("failed get cart")])
        );
      }
    } catch (error) {
      return res.json(new ResponseData(400, null, null, error));
    }
  }
);

router.post("/", validation, async (req: ICartRequest, res: Response) => {
  try {
    const store = await Store.create(req.body);
    if (store != null) {
      logger.info(JSON.stringify(new Log(req.originalUrl, req.method, store)));
      return res
        .status(200)
        .json(new ResponseData(200, new Data(["store"], [store])));
    } else {
      return res
        .status(200)
        .json(
          new ResponseData(400, null, null, [new Error("failed create store")])
        );
    }
  } catch (error) {
    logger.warn(JSON.stringify(new Log(req.originalUrl, req.method, error)));
    return res.status(200).json(new ResponseData(400, null, null, error));
  }
});


router.put(
  "/:id",
  validationUUID,
  validation,
  async (req: ICartRequest, res: Response) => {
    try {
      const data = await Store.findByPk(req.params.id);
      if (data == null) {
        return res.json(
          new ResponseData(400, null, null, [new Error("store not found")])
        );
      }

      const store = await Store.update(req.body, {
        where: {
          id: req.params.id,
        },
        returning: true,
      });
      if (store != null) {
        logger.info(
          JSON.stringify(new Log(req.originalUrl, req.method, store[1][0]))
        );
        return res
          .status(200)
          .json(new ResponseData(200, new Data(["store"], [store[1][0]])));
      } else {
        return res
          .status(200)
          .json(
            new ResponseData(400, null, null, [
              new Error("failed update Store"),
            ])
          );
      }
    } catch (error) {
      logger.warn(JSON.stringify(new Log(req.originalUrl, req.method, error)));
      return res.status(200).json(new ResponseData(400, null, null, error));
    }
  }
);

router.delete(
  "/:id",
  validationUUID,
  async (req: ICartRequest, res: Response) => {
    try {
      const data = await Store.findByPk(req.params.id);
      if (data == null) {
        return res.json(
          new ResponseData(400, null, null, [new Error("store not found")])
        );
      }

      const store = await Store.destroy({
        where: {
          id: req.params.id,
        },
      });

      if (store != null) {
        logger.info(JSON.stringify(new Log(req.originalUrl, req.method, data)));
        return res
          .status(200)
          .json(new ResponseData(200, new Data(["store"], [store])));
      } else {
        return res
          .status(200)
          .json(
            new ResponseData(400, null, null, [
              new Error("failed delete Store"),
            ])
          );
      }
    } catch (error) {
      logger.warn(JSON.stringify(new Log(req.originalUrl, req.method, error)));
      return res.status(200).json(new ResponseData(400, null, null, error));
    }
  }
);

async function validation(
  req: ICartRequest,
  res: Response,
  next: (error?: any) => void
) {
//   Promise.allSettled([
//     await body("name").notEmpty().withMessage("Name is required").run(req),
//     await body("tag").notEmpty().withMessage("Tag is required").run(req),
//     await body("reportTypeId")
//       .notEmpty()
//       .withMessage("Report Type Id is required")
//       .isUUID()
//       .withMessage("Report Type Id is UUID")
//       .custom(async (value) => {
//         try {
//           const reportType = await ReportType.findOne({
//             where: {
//               id: value,
//             },
//           });
//           if (reportType == null) {
//             throw "Report Type not found";
//           }
//         } catch (error) {
//           throw "Report Type not found";
//         }

//         return true;
//       })
//       .run(req),
//   ]);

  const result = validationResult(req);

  if (!result.isEmpty()) {
    return res.json(new ResponseData(400, null, null, result.array()));
  }

  return next();
}

export default router;
