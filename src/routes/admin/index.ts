import { Router } from "express";
import userAdminRouter from "@routes/admin/UserAdmin.routes";
import userRouter from "@routes/admin/User.routes";
// import reportTypeRouter from "@routes/admin/ReportType.routes";
// import storeRouter from "@routes/admin/Store.routes";
// import reportRouter from "@routes/admin/Report.routes";
// import masterDataRouter from "@routes/admin/MasterData.routes";
import masterProductRouter from "@routes/admin/MasterProduct.routes";
import { isAdmin } from "@middleware/Auth";

// Init router and path
const router = Router();

// Add sub-routes
router.use("/adminUser", isAdmin, userAdminRouter);
router.use("/user", isAdmin, userRouter);
// router.use("/reportType", isAdmin, reportTypeRouter);
router.use("/masterProduct", isAdmin, masterProductRouter);
// router.use("/store", isAdmin, storeRouter);
// router.use("/report", reportRouter);
// router.use("/masterData", masterDataRouter);

// Export the base-router
export default router;
