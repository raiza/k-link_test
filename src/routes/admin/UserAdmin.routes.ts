import { Op } from "sequelize";
import { IQueryRequest } from "@entities/common/Request";
import Data from "@entities/common/Data";
import ResponseData from "@entities/common/ResponseData";
import { generateHash } from "@shared/functions";
import { Response, Router } from "express";
import { body, param, validationResult } from "express-validator";
import Admin from "@models/Admin.model";
import {
  validationChangePassword,
  validationUUID,
} from "@middleware/Validation";
import { caching, saveCache } from "@middleware/Redis";
import { Params, Request } from "@entities/common/types";
import Page from "@entities/common/Page";
import { IRequestAdmin } from "@entities/Admin";
import { IChangePasswordRequest } from "@entities/common/ChangePassword";
import logger from "@shared/Logger";
import Log from "@entities/common/Log";
import { Sequelize } from "sequelize-typescript";
const router = Router();

router.get(
  "/list",
  caching,
  async (req: Request<any, IQueryRequest, Params>, res: Response) => {
    try {
      var offset = 0;
      var limit = 10;
      var page = parseInt(req.query.page);
      var perPage = parseInt(req.query.perPage);
      var order: any = [];
      var where: any = {};

      if (perPage >= 1) {
        limit = perPage;
      }

      if (page > 1) {
        offset = page - 1;
        offset = offset * limit;
      }

      if (req.query.orderBy != null && req.query.orderBy.length > 0) {
        order = req.query.orderBy.map((ordering) => [
          ordering.split(" ")[0],
          ordering.split(" ")[1],
        ]);
      }

      if (req.query.q != null) {
        where[Op.or] = [
          Sequelize.literal(`"Admin".*::TEXT ILIKE '%${req.query.q}%'`),
        ];
      }

      const { rows, count } = await Admin.findAndCountAll({
        offset: offset,
        limit: limit,
        order: order,
        where,
      });

      if (rows != null && rows.length > 0) {
        const data = new Data(
          ["admins"],
          [rows],
          new Page(offset, limit, count)
        );
        saveCache(req, 5, JSON.stringify(data));
        return res.status(200).json(new ResponseData(200, data));
      } else {
        return res.json(
          new ResponseData(400, null, null, [new Error("Data not found")])
        );
      }
    } catch (error) {
      return res.json(new ResponseData(400, null, null, error));
    }
  }
);

router.get(
  "/:id",
  validationUUID,
  caching,
  async (req: Request, res: Response) => {
    try {
      const admin = await Admin.findByPk(req.params.id);
      if (admin != null) {
        const data = new Data(["admin"], [admin]);
        saveCache(req, 5, JSON.stringify(data));

        return res.status(200).json(new ResponseData(200, data));
      } else {
        return res
          .status(200)
          .json(
            new ResponseData(400, null, null, [new Error("failed get Admin")])
          );
      }
    } catch (error) {
      return res.json(new ResponseData(400, null, null, error));
    }
  }
);

router.post("/", validation, async (req: IRequestAdmin, res: Response) => {
  try {
    const admin = await Admin.create(req.body);
    if (admin != null) {
      logger.info(JSON.stringify(new Log(req.originalUrl, req.method, admin)));
      return res
        .status(200)
        .json(new ResponseData(200, new Data(["admin"], [admin])));
    } else {
      return res
        .status(200)
        .json(
          new ResponseData(400, null, null, [new Error("failed create Admin")])
        );
    }
  } catch (error) {
    logger.warn(JSON.stringify(new Log(req.originalUrl, req.method, error)));
    return res.status(200).json(new ResponseData(400, null, null, error));
  }
});

router.put(
  "/changePassword",
  validationChangePassword,
  async (req: IChangePasswordRequest, res: Response) => {
    try {
      const data = await Admin.findByPk(req.admins.id);
      if (data == null) {
        return res.json(
          new ResponseData(400, null, null, [new Error("Admin not found")])
        );
      }

      const admin = await Admin.update(
        {
          password: req.body.newPassword,
        },
        {
          where: {
            id: req.admins.id,
          },
          returning: true,
        }
      );

      if (admin != null) {
        logger.info(
          JSON.stringify(new Log(req.originalUrl, req.method, admin[1][0]))
        );
        return res.json(
          new ResponseData(200, new Data(["admin"], [admin[1][0]]))
        );
      } else {
        return res.json(
          new ResponseData(400, null, null, [
            new Error("failed change password Admin"),
          ])
        );
      }
    } catch (error) {
      logger.warn(JSON.stringify(new Log(req.originalUrl, req.method, error)));
      return res.status(200).json(new ResponseData(400, null, null, error));
    }
  }
);

router.put(
  "/:id",
  validationUUID,
  validation,
  async (req: IRequestAdmin, res: Response) => {
    try {
      const data = await Admin.findByPk(req.params.id);
      if (data == null) {
        return res.json(
          new ResponseData(400, null, null, [new Error("Admin not found")])
        );
      }

      const admin = await Admin.update(req.body, {
        where: {
          id: req.params.id,
        },
        returning: true,
      });

      if (admin != null) {
        logger.info(
          JSON.stringify(new Log(req.originalUrl, req.method, admin[1][0]))
        );
        return res.json(
          new ResponseData(200, new Data(["admin"], [admin[1][0]]))
        );
      } else {
        return res.json(
          new ResponseData(400, null, null, [new Error("failed update Admin")])
        );
      }
    } catch (error) {
      logger.warn(JSON.stringify(new Log(req.originalUrl, req.method, error)));
      return res.status(200).json(new ResponseData(400, null, null, error));
    }
  }
);

router.delete(
  "/:id",
  validationUUID,
  async (req: IRequestAdmin, res: Response) => {
    try {
      const data = await Admin.findByPk(req.params.id);
      if (data == null) {
        return res.json(
          new ResponseData(400, null, null, [new Error("Admin not found")])
        );
      }

      const admin = await Admin.destroy({
        where: {
          id: req.params.id,
        },
      });
      if (admin != null) {
        logger.info(JSON.stringify(new Log(req.originalUrl, req.method, data)));
        return res
          .status(200)
          .json(new ResponseData(200, new Data(["admin"], [admin])));
      } else {
        return res
          .status(200)
          .json(
            new ResponseData(400, null, null, [
              new Error("failed delete Admin"),
            ])
          );
      }
    } catch (error) {
      logger.warn(JSON.stringify(new Log(req.originalUrl, req.method, error)));
      return res.status(200).json(new ResponseData(400, null, null, error));
    }
  }
);

async function validation(
  req: IRequestAdmin,
  res: Response,
  next: (error?: any) => void
) {
  Promise.allSettled([
    await body("email")
      .isEmail()
      .withMessage("Email not valid")
      .if(param("id").not().exists())
      .custom(async (value) => {
        try {
          const admin = await Admin.findOne({
            where: {
              email: value,
            },
          });
          if (admin != null) {
            throw "Email Already exists";
          }
        } catch (error) {
          if (value == null) {
            throw "Email is required";
          } else {
            throw "Email Already exists";
          }
        }

        return true;
      })
      .if(param("id").exists())
      .custom(async (value) => {
        try {
          const admin = await Admin.findOne({
            where: {
              email: value,
            },
          });
          if (admin != null && admin.id != req.params.id) {
            throw "Email Already exists";
          }
        } catch (error) {
          if (value == null) {
            throw "Email is required";
          } else {
            throw error;
          }
        }

        return true;
      })
      .toLowerCase()
      .run(req),
    await body("name").notEmpty().withMessage("Name required").run(req),
  ]);

  const result = validationResult(req);

  if (!result.isEmpty()) {
    return res.json(new ResponseData(400, null, null, result.array()));
  }

  if (req.params.id == null) {
    Promise.all([
      await body("password")
        .isLength({ min: 6 })
        .withMessage("The password min 6 characters")
        .run(req),
    ]);

    const result = validationResult(req);

    if (!result.isEmpty()) {
      return res.json(new ResponseData(400, null, null, result.array()));
    }

    const password = await generateHash(req.body.password);
    req.body.password = password;
  } else {
    delete req.body.password;
  }

  return next();
}

export default router;
