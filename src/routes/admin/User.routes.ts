import { Sequelize } from "sequelize-typescript";
import { validationUUID } from "@middleware/Validation";
// import ReportType from "@models/ReportType.model";
import Data from "@entities/common/Data";
import ResponseData from "@entities/common/ResponseData";
import { IQueryRequestUser, IRequestUser } from "@entities/User";
import { UserType } from "@shared/constant";
import { generateHash, randomValueHex } from "@shared/functions";
import { Response, Router } from "express";
import { body, param, validationResult } from "express-validator";
import User from "@models/User";
import Error from "@entities/common/Error";
// import ShareEntity from "@entities/Share";
// import Share from "@models/Share.model";
import { caching, saveCache } from "@middleware/Redis";
import { Params, Request } from "@entities/common/types";
import Page from "@entities/common/Page";
import logger from "@shared/Logger";
import Log from "@entities/common/Log";
import { sendEmailHtml } from "@gmail";
import { Op } from "sequelize";

const router = Router();

router.get(
  "/list",
  caching,
  async (req: Request<any, IQueryRequestUser, Params>, res: Response) => {
    try {
      var offset = 0;
      var limit = 10;
      var page = parseInt(req.query.page);
      var perPage = parseInt(req.query.perPage);
      var where: any = {};
      var order: any = [];

      if (perPage >= 1) {
        limit = perPage;
      }

      if (page > 1) {
        offset = page - 1;
        offset = offset * limit;
      }

      if (req.query.type != null) {
        where.type = req.query.type;
      }

      if (req.query.userId != null) {
        where.userId = req.query.userId;
      }

      if (req.query.q != null) {
        where[Op.or] = [
          Sequelize.literal(`"User".*::TEXT ILIKE '%${req.query.q}%'`),
          Sequelize.literal(`"user"."name" ILIKE '%${req.query.q}%'`),
        ];
      }

      if (req.query.orderBy != null && req.query.orderBy.length > 0) {
        order = req.query.orderBy.map((ordering) => [
          ordering.split(" ")[0],
          ordering.split(" ")[1],
        ]);
      }

      const { rows, count } = await User.findAndCountAll({
        offset: offset,
        limit: limit,
        where,
        order: order,
        attributes: {
          include: [[Sequelize.literal('"user"."name"'), "labelName"]],
        },
        include: [
          {
            model: User,
            attributes: [],
          },
        ],
      });

      if (rows != null && rows.length > 0) {
        const data = new Data(
          ["users"],
          [rows],
          new Page(offset, limit, count)
        );
        saveCache(req, 5, JSON.stringify(data));
        return res.status(200).json(new ResponseData(200, data));
      } else {
        return res.json(
          new ResponseData(400, null, null, [new Error("Data not found")])
        );
      }
    } catch (error) {
      return res.json(new ResponseData(400, null, null, error));
    }
  }
);

// router.get(
//   "/:id",
//   validationUUID,
//   caching,
//   async (req: IRequestUser, res: Response) => {
//     try {
//       const user = await User.findByPk(req.params.id, {
//         attributes: {
//           include: [[Sequelize.literal('"user"."name"'), "labelName"]],
//         },
//         include: [
//           {
//             model: User,
//             attributes: [],
//           },
//           {
//             model: Share,
//           },
//         ],
//       });
//       if (user != null) {
//         const data = new Data(["user"], [user]);
//         saveCache(req, 5, JSON.stringify(data));

//         return res.status(200).json(new ResponseData(200, data));
//       } else {
//         return res
//           .status(200)
//           .json(
//             new ResponseData(400, null, null, [new Error("failed get User")])
//           );
//       }
//     } catch (error) {
//       return res.json(new ResponseData(400, null, null, error));
//     }
//   }
// );

// router.post("/", validation, async (req: IRequestUser, res: Response) => {
//   try {
//     const random = randomValueHex(6);
//     const password = await generateHash(random);
//     req.body.password = password;

//     const user = await User.create(req.body);
//     if (user != null) {
//       if (req.body.shares != null && req.body.shares.length > 0) {
//         const shares = req.body.shares.map(
//           (share) =>
//             new ShareEntity(
//               share.labelShare,
//               share.userShare,
//               user.id,
//               share.reportTypeId
//             )
//         );

//         logger.info(
//           JSON.stringify(new Log(req.originalUrl, req.method, shares))
//         );
//         await Share.bulkCreate(shares);
//       }

//       let htmlBody = `
//       <p>Congratulations, your account has been successfully registered on Lifeline. Here is your password</p>
//       <p>&nbsp;</p>
//       <h3><b>${random}</b></h3>
//       <p>&nbsp;</p>
//       <p>Make sure to login immediately and immediately change your password</p>
//       `;

//       let send = sendEmailHtml(
//         req.body.email,
//         "Your email has been registered successfully",
//         htmlBody
//       );

//       logger.info(JSON.stringify(new Log(req.originalUrl, req.method, user)));
//       return res
//         .status(200)
//         .json(new ResponseData(200, new Data(["user"], [user])));
//     } else {
//       return res
//         .status(200)
//         .json(
//           new ResponseData(400, null, null, [new Error("failed create User")])
//         );
//     }
//   } catch (error) {
//     logger.warn(JSON.stringify(new Log(req.originalUrl, req.method, error)));
//     return res.status(200).json(new ResponseData(400, null, null, error));
//   }
// });

// router.put(
//   "/changePassword/:id",
//   validationChangePassword,
//   async (req: IChangePasswordRequest, res: Response) => {
//     try {
//       const data = await User.findByPk(req.params.id);
//       if (data == null) {
//         return res.json(
//           new ResponseData(400, null, null, [new Error("User not found")])
//         );
//       }

//       const user = await User.update(
//         {
//           password: req.body.newPassword,
//         },
//         {
//           where: {
//             id: req.params.id,
//           },
//           returning: true,
//         }
//       );

//       if (user != null) {
//         return res.json(
//           new ResponseData(200, new Data(["user"], [user[1][0]]))
//         );
//       } else {
//         return res.json(
//           new ResponseData(400, null, null, [new Error("failed change password User")])
//         );
//       }
//     } catch (error) {
//       return res.status(200).json(new ResponseData(400, null, null, error));
//     }
//   }
// );

// router.put(
//   "/:id",
//   validationUUID,
//   validation,
//   async (req: IRequestUser, res: Response) => {
//     try {
//       const data = await User.findByPk(req.params.id);
//       if (data == null) {
//         return res.json(
//           new ResponseData(400, null, null, [new Error("User not found")])
//         );
//       }

//       const user = await User.update(req.body, {
//         where: {
//           id: req.params.id,
//         },
//         returning: true,
//       });

//       if (user != null) {
//         let removeShare = await Share.destroy({
//           where: {
//             userId: user[1][0].id,
//           },
//         });

//         if (req.body.shares != null && req.body.shares.length > 0) {
//           const shares = req.body.shares.map(
//             (share) =>
//               new ShareEntity(
//                 share.labelShare,
//                 share.userShare,
//                 user[1][0].id,
//                 share.reportTypeId
//               )
//           );

//           logger.info(
//             JSON.stringify(new Log(req.originalUrl, req.method, shares))
//           );
//           await Share.bulkCreate(shares, {
//             updateOnDuplicate: ["id"],
//           });
//         }
//         logger.info(
//           JSON.stringify(new Log(req.originalUrl, req.method, user[1][0]))
//         );
//         return res.json(
//           new ResponseData(200, new Data(["user"], [user[1][0]]))
//         );
//       } else {
//         return res.json(
//           new ResponseData(400, null, null, [new Error("failed update User")])
//         );
//       }
//     } catch (error) {
//       logger.warn(JSON.stringify(new Log(req.originalUrl, req.method, error)));
//       return res.status(200).json(new ResponseData(400, null, null, error));
//     }
//   }
// );

router.delete(
  "/:id",
  validationUUID,
  async (req: IRequestUser, res: Response) => {
    try {
      const data = await User.findByPk(req.params.id);
      if (data == null) {
        return res.json(
          new ResponseData(400, null, null, [new Error("User not found")])
        );
      }

      const user = await User.destroy({
        where: {
          id: req.params.id,
        },
      });
      if (user != null) {
        logger.warn(JSON.stringify(new Log(req.originalUrl, req.method, data)));
        return res
          .status(200)
          .json(new ResponseData(200, new Data(["user"], [user])));
      } else {
        return res
          .status(200)
          .json(
            new ResponseData(400, null, null, [new Error("failed delete User")])
          );
      }
    } catch (error) {
      logger.warn(JSON.stringify(new Log(req.originalUrl, req.method, error)));
      return res.status(200).json(new ResponseData(400, null, null, error));
    }
  }
);

async function validation(
  req: IRequestUser,
  res: Response,
  next: (error?: any) => void
) {
  Promise.allSettled([
    await body("email")
      .isEmail()
      .withMessage("Email not valid")
      .if(param("id").not().exists())
      .custom(async (value) => {
        try {
          const user = await User.findOne({
            where: {
              email: value,
            },
          });
          if (user != null) {
            throw "Email Already exists";
          }
        } catch (error) {
          if (value == null) {
            throw "Email is required";
          } else {
            throw "Email Already exists";
          }
        }

        return true;
      })
      .if(param("id").exists())
      .custom(async (value) => {
        try {
          const user = await User.findOne({
            where: {
              email: value,
            },
          });
          if (user != null && user.id != req.params.id) {
            throw "Email Already exists";
          }
        } catch (error) {
          if (value == null) {
            throw "Email is required";
          } else {
            throw "Email Already exists";
          }
        }

        return true;
      })
      .toLowerCase()
      .run(req),
    await body("name").notEmpty().withMessage("Name required").run(req),
    await body("identityNumber")
      .isLength({ min: 10, max: 20 })
      .withMessage("Identity Number min 10 and Max 20 digit")
      .run(req),
    await body("phoneNumber")
      .isLength({ min: 5, max: 16 })
      .withMessage("Phone Number min 5 and Max 16 digit")
      .run(req),
    await body("npwp")
      .isLength({ min: 20, max: 20 })
      .withMessage("NPWP Must be 20 digits")
      .run(req),
    await body("CAN")
      .notEmpty()
      .withMessage("Cooperation Agreement Number is required")
      .run(req),
    await body("accountNumber")
      .notEmpty()
      .withMessage("Bank Account Number is required")
      .run(req),
    await body("accountName")
      .isLength({ min: 5, max: 16 })
      .withMessage("Bank Account Name is required")
      .run(req),
    await body("type")
      .custom((value) => {
        if (!Object.values(UserType).includes(value)) {
          throw "type must label,artist,producer or broker";
        }

        return true;
      })
      .run(req),
  ]);

  const result = validationResult(req);

  if (!result.isEmpty()) {
    return res.json(new ResponseData(400, null, null, result.array()));
  }

  if (req.body.type != UserType.LABEL) {
    Promise.allSettled([
      await body("userId")
        .notEmpty()
        .isUUID()
        .withMessage("User Id is UUID")
        .withMessage("User Id is Required")
        .run(req),
    ]);

    const result = validationResult(req);

    if (!result.isEmpty()) {
      return res.json(new ResponseData(400, null, null, result.array()));
    }

    const user = await User.findByPk(req.body.userId);
    if (user == null) {
      return res.json(
        new ResponseData(400, null, null, [
          new Error("User Id not found", "userId", req.body.userId),
        ])
      );
    }

  } else {
    delete req.body.userId;

  }

  const resultLast = validationResult(req);

  if (!resultLast.isEmpty()) {
    return res.json(new ResponseData(400, null, null, resultLast.array()));
  }

  return next();
}

export default router;
