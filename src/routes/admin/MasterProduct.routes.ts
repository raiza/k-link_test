import logger from '@shared/Logger';
import ReportType from "@models/MasterProduct.model";
import { IMasterProductRequest } from "@entities/MasterProduct";
import Data from "@entities/common/Data";
import ResponseData from "@entities/common/ResponseData";
import { IRequestUser } from "@entities/User";
import { Response, Router } from "express";
import { body, param, validationResult } from "express-validator";
import { caching, saveCache } from "@middleware/Redis";
import { Params, Request } from "@entities/common/types";
import { IQueryRequest } from "@entities/common/Request";
import Page from "@entities/common/Page";
import Error from "@entities/common/Error";
import { validationUUID } from "@middleware/Validation";
import Log from "@entities/common/Log";
import { Sequelize } from 'sequelize-typescript';
import { Op } from 'sequelize';
const router = Router();

router.get(
  "/list",
  caching,
  async (req: Request<any, IQueryRequest, Params>, res: Response) => {
    try {
      var offset = 0;
      var limit = 10;
      var page = parseInt(req.query.page);
      var perPage = parseInt(req.query.perPage);
      var order: any = []
      var where: any = {}

      if (perPage >= 1) {
        limit = perPage;
      }

      if (page > 1) {
        offset = page - 1;
        offset = offset * limit;
      }

      if (req.query.q != null) {
        where[Op.or] = [
          Sequelize.literal(`"ReportType".*::TEXT ILIKE '%${req.query.q}%'`),
        ];
      }

      if (req.query.orderBy != null && req.query.orderBy.length > 0) {
        order = req.query.orderBy.map(ordering => [ordering.split(" ")[0],ordering.split(" ")[1]])
      }

      const {rows, count} = await ReportType.findAndCountAll({
        offset: offset,
        limit: limit,
        order: order,
        where
      })

      if (rows != null && rows.length > 0) {
        const data = new Data(
          ["reportTypes"],
          [rows],
          new Page(offset, limit, count)
        );
        saveCache(req, 5, JSON.stringify(data));
        return res.status(200).json(new ResponseData(200, data));
      } else {
        return res
          .status(200)
          .json(
            new ResponseData(400, null, null, [new Error("Data not found")])
          );
      }
    } catch (error) {
      return res.json(new ResponseData(400, null, null, error))
    }
  }
);

router.get(
  "/:id",
  validationUUID,
  caching,
  async (req: IMasterProductRequest, res: Response) => {
    try {
      const reportType = await ReportType.findByPk(req.params.id);
      if (reportType != null) {
        const data = new Data(["reportType"], [reportType]);
        saveCache(req, 5, JSON.stringify(data));

        return res
          .status(200)
          .json(new ResponseData(200, data));
      } else {
        return res
          .status(200)
          .json(
            new ResponseData(400, null, null, [
              new Error("failed get Report Type"),
            ])
          );
      }
    } catch (error) {
      return res.status(200).json(new ResponseData(400, null, null, error));
    }
  }
);

router.post("/", validation, async (req: IMasterProductRequest, res: Response) => {
  try {
    const reportType = await ReportType.create(req.body);
    if (reportType != null) {
      logger.info(JSON.stringify(new Log(req.originalUrl,req.method,reportType)));
      return res
        .status(200)
        .json(new ResponseData(200, new Data(["reportType"], [reportType])));
    } else {
      return res
        .status(200)
        .json(
          new ResponseData(400, null, null, [
            new Error("failed create Report Type"),
          ])
        );
    }
  } catch (error) {
    logger.warn(JSON.stringify(new Log(req.originalUrl,req.method,error)));
    return res.status(200).json(new ResponseData(400, null, null, error));
  }
});

router.put(
  "/:id",
  validationUUID,
  validation,
  async (req: IMasterProductRequest, res: Response) => {
    try {
      const data = await ReportType.findByPk(req.params.id);
      if (data == null) {
        return res.json(
          new ResponseData(400, null, null, [
            new Error("Report Type not found"),
          ])
        );
      }

      const reportType = await ReportType.update(req.body, {
        where: {
          id: req.params.id,
        },
        returning: true,
      });
      if (reportType != null) {
      logger.info(JSON.stringify(new Log(req.originalUrl,req.method,reportType[1][0])));
      return res
          .status(200)
          .json(
            new ResponseData(200, new Data(["reportType"], [reportType[1][0]]))
          );
      } else {
        return res
          .status(200)
          .json(
            new ResponseData(400, null, null, [
              new Error("failed update Report Type"),
            ])
          );
      }
    } catch (error) {
      logger.warn(JSON.stringify(new Log(req.originalUrl,req.method,error)));
      return res.status(200).json(new ResponseData(400, null, null, error));
    }
  }
);

router.delete(
  "/:id",
  validationUUID,
  async (req: IMasterProductRequest, res: Response) => {
    try {
      const data = await ReportType.findByPk(req.params.id);
      if (data == null) {
        return res.json(
          new ResponseData(400, null, null, [
            new Error("Report Type not found"),
          ])
        );
      }

      const reportType = await ReportType.destroy({
        where: {
          id: req.params.id,
        },
      });
      if (reportType != null) {
      logger.info(JSON.stringify(new Log(req.originalUrl,req.method,data)));
      return res
          .status(200)
          .json(new ResponseData(200, new Data(["reportType"], [reportType])));
      } else {
        return res
          .status(200)
          .json(
            new ResponseData(400, null, null, [
              new Error("failed delete Report Type"),
            ])
          );
      }
    } catch (error) {
      logger.warn(JSON.stringify(new Log(req.originalUrl,req.method,error)));
      return res.status(200).json(new ResponseData(400, null, null, error));
    }
  }
);

async function validation(
  req: IMasterProductRequest,
  res: Response,
  next: (error?: any) => void
) {
  // Promise.allSettled([
  //   await body("name").notEmpty().withMessage("Name is required").run(req),
  // ]);

  const result = validationResult(req);

  if (!result.isEmpty()) {
    return res
      .json(new ResponseData(400, null, null, result.array()));
  }

  return next();
}

export default router;
