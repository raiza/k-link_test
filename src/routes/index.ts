import { Router } from "express";
// import storeRouter from "@routes/Store.routes";
import userRouter from "@routes/User.routes";
// import reportUserRouter from "@routes/ReportUser.routes";
import cartRouter from "@routes/Cart.routes";
import adminRouter from "@routes/admin";
import { isAdmin, isUser } from "@middleware/Auth";

// Init router and path
const router = Router();
router.use("/admin", isAdmin, adminRouter);
// router.use("/store", isUser, storeRouter);
router.use("/user", isUser, userRouter);
router.use("/cart", isUser, cartRouter);
// router.use("/reportUser", isUser, reportUserRouter);

// Export the base-router
export default router;
