import jwt from 'jsonwebtoken';
import Data from "@entities/common/Data";
import { Request, Response, Router } from "express";
import passport from "passport";
import ResponseData from "@entities/common/ResponseData";
import { IUser } from "@entities/User";
const router = Router();

router.post("/login", (req: Request, res: Response, next) => {
  passport.authenticate("user-local", function (err, user:IUser) {
    if (!user) {
      return res.status(200).json(new ResponseData(400, null, null, err));
    }

    const token = jwt.sign({id:user.id,type:"user"}, process.env.SECRET,{ expiresIn: '1d' })

    return res.status(200).json(new ResponseData(200, new Data(["user","token"], [user,token])));
  })(req, res, next);
});

router.post("/admin/login", (req: Request, res: Response, next) => {
  passport.authenticate("admin-local", function (err, user) {
    if (!user) {
      return res.status(200).json(new ResponseData(400, null, null, err));
    }

    const token = jwt.sign({id:user.id,type:"admin"}, process.env.SECRET,{ expiresIn: '1d' })
  
    return res.status(200).json(new ResponseData(200, new Data(["admin","token"], [user,token])));
  })(req, res, next);
});

export default router;
