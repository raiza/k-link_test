import Admin from "@models/Admin.model";
import { IDb } from "@entities/common/Db";
import { Sequelize } from "sequelize-typescript";
import User from "@models/User";
// import Share from "@models/Share.model";
// import ReportType from "@models/ReportType.model";
// import Store from "@models/Store.model";
// import Report from "@models/Report.model";
// import MasterData from "@models/MasterData.model";
import MasterProduct from "@models/MasterProduct.model";
// import ReportUser from "@models/ReportUser.model";
import Cart from "@models/Cart.model";
import Checkout from "@models/Checkout.model";

const db: IDb = { sql: null };

const sequelize = new Sequelize(
  process.env.DB_DATABASE,
  process.env.DB_USERNAME,
  process.env.DB_PASSWORD,
  {
    models: [
      User,
      Admin,
      MasterProduct, 
      Cart,
      Checkout,
    ],
    dialect: "postgres",
    host: process.env.DB_HOST,
    ssl: false,
    port: 5432,
    pool: {
      max: 80,
      min: 5,
      acquire: 30000,
      idle: 10000,
    },
  }
);

db.sql = sequelize;

export default db;
