import nodemailer from "nodemailer";
import GmailTransport from "gmail-nodemailer-transport";
import logger from "@shared/Logger";
import Log from "@entities/common/Log";
export const sendEmailHtml = async (to: string, subject: string,body: string) => {
  let transport = nodemailer.createTransport(
    new GmailTransport({
      userId: process.env.EMAIL,
      auth: {
        clientId: process.env.CLIENT_ID,
        clientSecret: process.env.CLIENT_SECRET,
        accessToken: process.env.ACCESS_TOKEN,
        refreshToken: process.env.REFRESH_TOKEN,
      },
    })
  );

  try {
    transport.sendMail({
      from: `Lifeline Register <${process.env.EMAIL}>`,
      to:to,
      subject: subject,
      html:body
    });

    return true
  } catch (error) {
    logger.warn(
      JSON.stringify(
        JSON.stringify(new Log("sendEmail", "post", error))
      )
    );
    return false
  }
};
