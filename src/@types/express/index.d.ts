import { IAdmin } from "@entities/Admin";
import { IUser } from "@entities/User";

declare global {
  declare namespace Express {

    interface Request {
      users?: IUser, //or other type you would like to use
      admins?: IAdmin
    }
  }
}