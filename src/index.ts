import './pre-start'; // Must be the first import
import app from '@server';
import logger from '@shared/Logger';
import db from "@database";
// Start the server
const port = Number(process.env.PORT || 3000);



db.sql.sync().then(() => {
    app.listen(port, () => {
        logger.info('Express server started on port: ' + port);
    });
})