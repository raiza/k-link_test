import { isLoggedInDownload } from './middleware/Auth';
import cookieSession from "express-session";
import cookieParser from "cookie-parser";
import morgan from "morgan";
import path from "path";
import helmet from "helmet";

import express, { NextFunction, Request, Response } from "express";
import StatusCodes from "http-status-codes";
import "express-async-errors";

import BaseRouter from "./routes";
import AuthRouter from "@routes/Auth.routes";
// import ReportUserRouter from "@routes/DownloadReportUser.routes";
import logger from "@shared/Logger";
import {
  isLoggedIn,
  localAuthUser,
  localAuthAdmin,
  bearerAuth,
  bearerAuthQuery,
} from "@middleware/Auth";
import passport from "passport";
import cors from "cors";

const app = express();
const { BAD_REQUEST } = StatusCodes;

/************************************************************************************
 *                              cors settings
 ***********************************************************************************/

var whitelist = ["http://localhost:3000", "http://localhost:3005"];
var corsOptions = {
  // allowedHeaders: [
  //   "Origin",
  //   "X-Requested-With",
  //   "Content-Type",
  //   "Accept",
  //   "X-Access-Token",
  // ],
  // credentials: true,
  // methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
  origin: "*",
};

app.use(cors());
/************************************************************************************
 *                              Set basic express settings
 ***********************************************************************************/

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

/************************************************************************************
 *                              session settings
 ***********************************************************************************/

app.use(
  cookieSession({
    secret: process.env.SECRET,
    cookie: {
      maxAge: 86400000,
    },
  })
);

/************************************************************************************
 *                              passport settings
 ***********************************************************************************/
app.use(passport.initialize());
app.use(passport.session());
localAuthUser(passport);
localAuthAdmin(passport);
bearerAuth(passport);
bearerAuthQuery(passport);

// Show routes called in console during development
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

// Security
if (process.env.NODE_ENV === "production") {
  app.use(helmet());
}

// Add APIs
app.use("/api/v1", isLoggedIn, BaseRouter);
app.use("/auth", AuthRouter);
// app.use("/reportUser", isLoggedInDownload, ReportUserRouter);

// Print API errors
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  logger.err(err, true);
  return res.status(BAD_REQUEST).json({
    error: err.message,
  });
});

/************************************************************************************
 *                              Serve front-end content
 ***********************************************************************************/

const viewsDir = path.join(__dirname, "views");
app.set("views", viewsDir);
const staticDir = path.join(__dirname, "public");
app.use(express.static(staticDir));

// Export express instance
export default app;
