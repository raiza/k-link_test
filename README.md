# Lifeline Webservice
### Before Running
```
$ npm install
```

### Running service local
```
$ npm run start:dev
```

### Running service local
```
$ screen -r PID
$ git pull origin master
$ npm run build
$ npm run start 
```

## Flow

```mermaid
    graph TD;
    login --> Admin;
    login --> User(Label, Artist, Producer, Broker);
    Admin --> User;
    Admin --> ReportType;
    Admin --> Stores;
    Admin --> UploadReport;
    User --> ChangePassword;
    User --> UpdateProfile;
    User --> GenerateReportByStore;
```
